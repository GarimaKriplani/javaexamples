package com.Testers;
import java.util.Arrays;
public class reverseWords {
    public static void main(String[] args) {
        char[] str = "Hello World".toCharArray();
        getReverse(str);
        System.out.println(str);

      }

    public static void getReverse(char[] str) {
        int n = str.length;
        int start = 0;
        for(int i = 0; i < n; i++) {
            if(str[i] ==  ' ' && i > 0) {
                System.out.println("in IF clause, i= "+i + "start= "+start);
                reverse(str, start, i-1);
                start = i+1;
            } else if(i == n-1) {
                System.out.println("In else clause, i= "+i);
                reverse(str, start, i);
            }
        }

    }

    private static void reverse(char[] str, int start, int end) {

        while(start < end) {
            swap(str, start, end);
            start++;
            end--;
        }
        System.out.println("returning from reverse, str =" +Arrays.toString(str) +" start=" +start+" end=" + end);
    }

    private static void swap(char[] str, int start, int end) {
        char tmp = str[start];
        str[start] = str[end];
        str[end] = tmp;
    }
}
