package com.Testers;
import java.io.UnsupportedEncodingException;
import java.nio.charset.*;
import java.util.*;
import java.util.Map.Entry;

public class StringManipulation {
    public static void permutate(String str) {
        permutation("", str);
    }

    private static void permutation(String ans, String remain) {
        int n = remain.length();
        // ans serves as an accumulator variable
        // base case: we print out ans since there are no more letters to be added
        if (n == 0) {
            System.out.println("ans="+ans);
        }
        // recursive case
        else {
            for (int i = 0; i < n; i++) {
                // we call permutation, each time combining prefix with the individual letters in str
                // we also remove these letters from str
                System.out.println("ans+remain.charAt( "+"i="+i +"): "+ans+remain.charAt(i));
                System.out.println("remain.substring(0,i) + remain.substring(i+1, n) : "+ remain.substring(0,i) +"="+ remain.substring(i+1, n));
                permutation(ans + remain.charAt(i), remain.substring(0,i) + remain.substring(i+1, n));
            }
        }
    }

    /* to sort string -method 1*/
    public static String sortString1(String input){
        char tem[]= input.toCharArray();
        Arrays.sort(tem);
        return new String(tem.toString());
    }

    /* to sort string -method 2 */

    public static String sortString2(String input){

        /*note*/
        Character temp[]= new Character[input.length()]; //or input.toCharArray()
        for(int i=0;i<temp.length;i++){
            temp[i]=input.charAt(i);

        }
        Arrays.sort(temp, new Comparator<Character>() {
            @Override
            public int compare(Character o1, Character o2) {
                return Character.compare(Character.toLowerCase(o1),Character.toLowerCase(o2));
            }
        });
        StringBuilder sb = new StringBuilder(temp.length);
        for (Character c : temp)
            sb.append(c.charValue());

        return sb.toString();

    }
    public static String reverseWord(String x) {
        // convert x into an arrayj
        char[] string = x.toCharArray();
        // create the StringBuffer object
        StringBuffer buffer = new StringBuffer();
        // iterate from the back of the array, each time adding a character to buffer
        for(int i = string.length - 1; i >= 0; i--) {
            buffer.append(string[i]);
        }
        return buffer.toString();
    }
    public static String reverseSentence (String sentence) {
        // first reverse the string x using the previous function
        sentence= reverseWord(sentence);
        System.out.println("sentence =" + sentence);
        // split x, effectively extracting the individual words
        String[] words = sentence.split(" ");
        // create a StringBuffer
        StringBuffer buffer = new StringBuffer();
        // reverse and add the entries in words to buffer
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
            buffer.append(reverseWord(words[i]) + " ");
        }
        return buffer.toString();
    }
    //func. to replace whitespaces in input string with a replacement string
    public static String replaceCorr(String input, String replacement){
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == ' ') {
                count++;
            }
        }
        // create an array, including the whitespace
        char[] word = new char[input.length() + count * replacement.length() - 1];
        int index = 0;
        //iterate through x, replacing whitespace
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == ' ') {
                for (int j = 0; j < replacement.length(); j++) {
                    word[index++] = replacement.charAt(j);
                }
            }
            else {
                word[index++] = input.charAt(i);
            }
        }
        return new String(word);
    }

    public static String replaceCorrBuffer(String input,String replace){
        //create a StringBuffer
        StringBuffer buffer = new StringBuffer();
        // iterate through x, replacing whitespace
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == ' ') {
                buffer.append(replace);
            }
            else {
                buffer.append(input.charAt(i));
            }
        }
        return buffer.toString();
    }

    public static String compressString(String x) {
        /* input x= aaabbcccc, output =a3b2c4*/
        // create a buffer
        StringBuffer buffer = new StringBuffer();
        // to hold the current alphabet
        char current = x.charAt(0);
        // to hold the current count
        int count = 1;
        // iterate through the array, counting
        for (int i = 1; i < x.length(); i++) {
            // update buffer if the letter has changed
            if (current != x.charAt(i)) {
                buffer.append(current);
                buffer.append(count);
                current = x.charAt(i);
                count = 1;
            }
            else {
                // increment the counter
                count++;
            }
        }
        // remember to add the last character
        buffer.append(current);
        buffer.append(count);
        // return a string
        return buffer.toString();
    }
    public static void main(String[] args){
        byte[] b_arr = {71, 101, 101, 107, 115};
        Charset cs = Charset.defaultCharset();
        String s_byte_char = new String(b_arr, cs);
       // System.out.println(s_byte_char); //Geeks

        byte[] b_arr1 = {71, 101, 101, 107, 115};
        try{
            String s1 = new String(b_arr1, 1, 4, "US-ASCII");
            //System.out.println(s1); //eeks
        }catch(UnsupportedEncodingException e){
            System.out.println(e);
        }
        permutate("abc");


        char char_arr[] = {'G', 'e', 'e', 'k', 's'};
        String s = new String(char_arr); //Geeks

        String a ="Hello", b="hello";
        int out = a.compareTo(b);
       // System.out.println("out=" +out);
        String abc ="Hello World";
        String res= reverseSentence(abc);
        System.out.println("reversed sentence ="+ res);

        System.out.println("---In string pattern matching---");
        PatternSearch2("This is test text","test");

    }
    public static void extraCodeforReference(){
        Map<Character, Integer> charMap = new HashMap<Character, Integer>();
        Set<Map.Entry<Character, Integer>> entrySet = charMap.entrySet();
        for (Map.Entry<Character, Integer> entry : entrySet)
        { if (entry.getValue() > 1) {
            System.out.printf("%s : %d %n", entry.getKey(), entry.getValue());
            }
        }


    }

    public static boolean checkAnagram(String first, String second){
        char[] characters = first.toCharArray();
        StringBuilder sbSecond = new StringBuilder(second);
        for(char ch : characters){
            int index = sbSecond.indexOf("" + ch);
            if(index != -1){
                sbSecond.deleteCharAt(index);
            }
            else{
                return false; }
        }
        return sbSecond.length()==0 ? true : false;
    }
//LinkedHashmap maintains the insertion order
  //  Read more: http://javarevisited.blogspot.com/2013/03/Anagram-how-to-check-if-two-string-are-anagrams-example-tutorial.html#ixzz5EUvF25m6
    public static char getFirstNonRepeatedChar(String str)
    {   Map<Character,Integer> counts = new LinkedHashMap<>(str.length());
        for (char c : str.toCharArray()) {
        counts.put(c, counts.containsKey(c) ? counts.get(c) + 1 : 1);
        }
        for (Entry<Character,Integer> entry : counts.entrySet()) {
        if (entry.getValue() == 1) {
            return entry.getKey();
            }
        } throw new RuntimeException("didn't find any non repeated Character");
    }
    /* * Finds first non repeated character in a String in just one pass.
     *  * It uses two storage to cut down one iteration, standard space vs time
     *  * trade-off.Since we store repeated and non-repeated character separately,
      *  * at the end of iteration, first element from List is our first non
      *  * repeated character from String. */
    public static char firstNonRepeatingChar(String word) {
        Set<Character> repeating = new HashSet<>();
        List<Character> nonRepeating = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            char letter = word.charAt(i);
            if (repeating.contains(letter))
            { continue;
            }
            if (nonRepeating.contains(letter)) {
                nonRepeating.remove((Character) letter);
                repeating.add(letter);
            }
            else {
                nonRepeating.add(letter);
            }
        }
        return nonRepeating.get(0);
    }
    // * Step 1 : Scan String and store count of each character in HashMap
    // * Step 2 : traverse String and get count for each character from Map.
    // * Since we are going through String from first to last character,
    // * when count for any character is 1, we break, it's the first * non repeated character.
    // Here order is achieved by going * through String again.
     public static char firstNonRepeatedCharacter(String word) {
        HashMap<Character,Integer> scoreboard = new HashMap<>();
        // build table [char -> count]
         for (int i = 0; i < word.length(); i++) {
             char c = word.charAt(i);
             if (scoreboard.containsKey(c)) {
                 scoreboard.put(c, scoreboard.get(c) + 1);
             }
             else {
                 scoreboard.put(c, 1);
             }
         } // since HashMap doesn't maintain order, going through string again
         for (int i = 0; i < word.length(); i++) {
             char c = word.charAt(i);
             if (scoreboard.get(c) == 1) {
                 return c;
             }
         } throw new RuntimeException("Undefined behaviour");
     }
    /* function to remove characters from a string recursively*/
    public static String removeRecursive(String word, char ch){
        int index = word.indexOf(ch);
        if(index == -1){
            return word;
        }
        return removeRecursive(word.substring(0, index) + word.substring(index +1, word.length()), ch);
    }

   /*
   Leetcode
   Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
   Example:
   Input: haystack = "hello", needle = "ll"
   Output: 2

   Input: haystack = "aaaaa", needle = "bba"
   Output: -1
    */
    public static int strStr(String haystack,String needle){
        int index=-1;
        if(needle.length() == 0)
            return 0;
        if(needle.length()>haystack.length())
            return -1;

        for(int i=0;i<=haystack.length();i++){
            if(i+needle.length()>haystack.length()){
                break;
            }
            int m=i;
            for(int j=0;j<needle.length();j++){
                if(needle.charAt(j)==haystack.charAt(m)){
                    if(j==needle.length()-1)
                        return i;
                    m++;
                }
                else
                    break;
            }
        }
        return  index;
    }


    /*
    KMP str str
    Leetcode-same as above function- strStr()
     */
    public static int strString(String haystack, String needle) {
        if(haystack==null || needle==null)
            return 0;

        int h = haystack.length();
        int n = needle.length();

        if (n > h)
            return -1;
        if (n == 0)
            return 0;

        int[] next = getNext(needle);
        int i = 0;

        while (i <= h - n) {
            int success = 1;
            for (int j = 0; j < n; j++) {
                if (needle.charAt(0) != haystack.charAt(i)) {
                    success = 0;
                    i++;
                    break;
                } else if (needle.charAt(j) != haystack.charAt(i + j)) {
                    success = 0;
                    i = i + j - next[j - 1];
                    break;
                }
            }
            if (success == 1)
                return i;
        }
        return -1;
    }
    //calculate KMP array
    public static int[] getNext(String needle) {
        int[] next = new int[needle.length()];
        next[0] = 0;

        for (int i = 1; i < needle.length(); i++) {
            int index = next[i - 1];
            while (index > 0 && needle.charAt(index) != needle.charAt(i)) {
                index = next[index - 1];
            }

            if (needle.charAt(index) == needle.charAt(i)) {
                next[i] = next[i - 1] + 1;
            } else {
                next[i] = 0;
            }
        }
        return next;
    }
    public static void PatternSearch(String txt, String pat){

        int M = pat.length();
        int N = txt.length();

        /* A loop to slide pat one by one */
        for (int i = 0; i <= N - M; i++) {
            int j;
            /* For current index i, check for pattern
              match */
            for (j = 0; j < M; j++) {
                if (txt.charAt(i + j) != pat.charAt(j)) {
                    System.out.println ("text.charAt(i+j) :  "+txt.charAt(i + j) + "!= pat.charAt(j): "+pat.charAt(j));
                    System.out.println("i= "+ i + "j= " +j);
                    break;
                }
            }

            if (j == M) // if pat[0...M-1] = txt[i, i+1, ...i+M-1]
                System.out.println("Pattern found at index i=" + i + "j= " +j + "M= "+M);
        }//end of for
    }

    /*
    LeetCode-
    Write a function to find the longest common prefix string amongst an array of strings.

    If there is no common prefix, return an empty string "".
    Example:
    Input: ["flower","flow","flight"]
    Output: "fl"

    Example:
    Input: ["dog","racecar","car"]
    Output: ""
    
     */
    public static String longestCommomPrefix(String[] str){
        return null;
    }
    public static void PatternSearch2(String text, String pattern){
        int M=pattern.length();
        int N=text.length();
        System.out.println("N= " +N);
        for (int i=0;i <=N-M;i++) {
             int j=0;
             if(text.charAt(i+j)==pattern.charAt(j)){
                 int startPos=i+j;
                 int endPos=startPos+M;
                 System.out.println("start Pos=" +startPos + " endPos= " + endPos);
                  if(pattern.equals(text.substring(startPos,endPos))){
                     System.out.println("found !!  at " +startPos);
                     break;
                 }
             }
        }
    }



}
