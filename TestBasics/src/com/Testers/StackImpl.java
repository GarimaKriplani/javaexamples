package com.Testers;
interface Stack<T> {
    Stack<T> push(T ele);
    T pop();
}
public class StackImpl {
    public static class StackArray<T> implements Stack<T>{
        private T[] arr;
        private int total;
        public StackArray(){
            arr = (T[]) new Object[2];
        }
        public StackArray<T> push (T item){
            if(arr.length ==total)
                resize(arr.length *2);
            arr[total++]=item;
            return this;
        }
        public void resize (int capacity){
            T[] tmp= (T[]) new Object[capacity];
            System.arraycopy(arr,0,tmp,0,total);
            arr=tmp;
        }
        public T pop(){
            if(total ==0 )
                return null;
            T item= arr[--total];
            arr[total] =null;
            if(total >0 && total == arr.length/4)
                resize(arr.length/2);
            return item;
        }
        @Override
        public String toString(){
           // return java.util.Arrays.toString(arr);
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<=total-1;i++){
                sb.append(arr[i]).append(", ");
            }
            return sb.toString();
        }
        public boolean isEmpty()
        {
            return (total == 0) ? true : false;
        }
        public T peek(){
            return arr[total-1];
        }
        public T top(){
            return arr[total-1];
        }
    }
    public static int prec(char a){
        switch(a){
            case'+':
            case'-':
                return 1;
            case '*':
            case '/':
                return 2;
            case '^':
                return  3;
        }
        return -1;

    }
    public static void deleteElementsThatAreSmaller(StackArray<Integer> stackArray,int [] input,int k){
        stackArray.push(input[0]);
        int count =0;
        for(int i=1;i<input.length;i++){
            while(!stackArray.isEmpty() && stackArray.top() < input[i]&& count<k ){
                stackArray.pop();
                count++;
            }
            stackArray.push(input[i]);
        }
        int m= stackArray.total ;
        int []v = new int[m];
        while(!stackArray.isEmpty()){
            v[--m]=stackArray.peek();
            stackArray.pop();
        }
        System.out.println("printing stack");
        for(int i:v)
            System.out.println(i + " ->");
    }
    public static void main(String []args){
       StackArray<Character> characterStackArray = new StackArray<Character>();
       StackArray<Integer> myIntStack = new StackArray<Integer>();
       System.out.println("Testing git PR, branching mechanism");
        int [] arr={20,10,25,30,40};
        int k=2;
        deleteElementsThatAreSmaller(myIntStack,arr,k);
    /* convert infix to postfix expression */
      String input="a+b*(c^d-e)^(f+g*h)-i";
      StringBuffer result= new StringBuffer();
      for(int i=0;i<input.length();i++){
          char c = input.charAt(i);
          if(Character.isLetterOrDigit(c)){
              result.append(c);
          }
          else if(c == '('){
              characterStackArray.push(c);
          }
          else if(c ==')'){
              while(!characterStackArray.isEmpty() && characterStackArray.peek()!='('){
                  result.append(characterStackArray.pop());
              }
              if(!characterStackArray.isEmpty() && characterStackArray.peek() != '('){
                  System.out.print("Invalid expr");
              }
              else
                  characterStackArray.pop();
          }
          else{
              while(!characterStackArray.isEmpty() && prec(c)<prec(characterStackArray.peek())){
                  result.append(characterStackArray.pop());
              }
              characterStackArray.push(c);
          }
      }
      while(!characterStackArray.isEmpty()){
          result.append(characterStackArray.pop());
      }
      System.out.println(result.toString());
      // System.out.println(characterStackArray.toString());
    }

    public class StackList<T> implements Stack<T>{
        private int total;
        private Node top;
        private class Node{
            T item;
            Node next;
        }
        public StackList(){

        }
        public StackList<T> push(T item){
            Node current=top;
            top = new Node();
            top.item=item;
            top.next=current;
            total++;
            return this;
        }
        public T pop(){
            if(top == null)
                return null;
            T curr=top.item;
            top=top.next;
            total--;
            return curr;
        }

        @Override
        public String toString(){
            StringBuilder sb = new StringBuilder();
            Node tmp=top;
            while(tmp!=null){
              sb.append(tmp.item).append(", ");
              tmp=tmp.next;
            }
            return sb.toString();
        }
    }


}
