package com.Testers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
public class MathProbs {
    public static int maxHeight(int arr[]){
        int res=1;
        for(int i=1;i<=arr.length;i++){
            long y= (i*(i+1))/2;
            if(y <arr.length){
                res=i;
            }
            else break;
        }
        return  res;
    }
    public static ArrayList<int[]> getPermutations(int []n,int k){
       //Set<int[]> resultSet= new HashSet<int[]>();
       ArrayList <int[]> resultSet = new ArrayList<int[]>();
        if(k==n.length){
            resultSet.add(n);
            for(int i=0;i<n.length;i++){
                System.out.print(n[i]+",");
            }
            System.out.println();
        }
        else{
            for(int i=k;i<n.length;i++){
                int temp = n[k];
                n[k] = n[i];
                n[i] = temp;
                getPermutations(n, k + 1);
                temp = n[k];
                n[k] = n[i];
                n[i] = temp;
            }
        }

        return resultSet;

    }

    public static void swapWithInt(int []n,int i,int j){
        int temp;
        temp=n[i];
        n[j]=n[i];
        n[i]=temp;

    }
    public static void printPermutations(String str, int l, int r){

        if(l ==r){
            System.out.println(str);
        }
        else{
            for(int i=l;i<=r;i++){
                str=swap(str,l,i);
                printPermutations(str,l+1,r);
                str=swap(str,l,i);
            }
        }
    }


    public static String swap(String s, int i,int j){
        char temp;
        char [] charArray=s.toCharArray();
        temp=charArray[i];
        charArray[i]=charArray[j];
        charArray[j]=temp;
        return String.valueOf(charArray);//charArray.toString();
    }
    public static  void main(String []args){
        int arr[] = { 40, 100, 20, 30 };
        int n[]={15,61,16};
        int res=maxHeight(arr);
        String s="ABC";
      ArrayList<int[]> res1= new ArrayList<int[]>();
        res1=getPermutations(n,0);
      //  System.out.println("size of resultSet ="+res1.);
        int []b= res1.get(3);
      //  printPermutations(s,0,s.length()-1);

       // System.out.print(res);
    }
}
