package com.Testers;
import java.util.Scanner;
/*
Write an algorithm that checks if original message is really encoded in final string. Given two string s and t ,write a code to decide whether s is a
subsequence of t i.e if you can remove characters from t such that the concatenation of remaining character is s.

Sample Input
This is the original message
At The Dis oisu utyhe orihjgin kal messinge age
 */

public class Test1 {
    public static void main(String args[]) throws Exception {
        Scanner scanner = new Scanner(System.in);
        /*System.out.println(" enter orginal message ");
        String original=scanner.nextLine();
        System.out.println("enter  encoded text");
        String encoded=scanner.nextLine();*/

        String original = "This is the original message";
        String encoded = "At The Dis oisu utyhe orihjgin kal messinge age";

        String target_tokens[] =original.split(" ");

        int tokenLength=target_tokens.length;

        for(int i=0,j=0;i<encoded.length() && j<tokenLength;i++){
            System.out.println("encoded.charAt(i) = " + encoded.charAt(i));
            System.out.println("target_tokens[j].charAt(0) = " + target_tokens[j].charAt(0));

            if(encoded.charAt(i) == target_tokens[j].charAt(0)){
                //first char matches
                //extract chars from encoded and match against target_tokens
                int len=target_tokens[j].length();
                String temp = encoded.substring(i,len);
                System.out.print("temp = " + temp);
                if(temp.equals(target_tokens[j])){
                    System.out.print("true");
                    j=j+1;
                }

            }
        }

    }
}
