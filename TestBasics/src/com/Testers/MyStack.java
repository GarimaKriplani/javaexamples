package com.Testers;
import java.util.Arrays;
import java.util.ArrayList;
public class MyStack {

    public static class MyStackArray<E> {
        private int size = 0;
        private static final int DEFAULT_CAPACITY = 10;
        private Object elements[];

        public MyStackArray() {
            elements = new Object[DEFAULT_CAPACITY];
        }

        public void push(E e) {
            if (size == elements.length) {
                ensureCapa();
            }
            elements[size++] = e;
        }

        @SuppressWarnings("unchecked")
        public E pop() {
            E e = (E) elements[--size];
            elements[size] = null;
            return e;
        }

        private void ensureCapa() {
            int newSize = elements.length * 2;
            elements = Arrays.copyOf(elements, newSize);
        }
    }


    public static class MyStackList<E> extends ArrayList<E> {

        private static final long serialVersionUID = 1L;

        public E pop() {
            E e = get(size() - 1);
            remove(size() - 1);
            return e;
        }

        public void push(E e) {
            add(e);
        }

    }

    public static void main(String[]args){
        MyStackArray<Integer> stackArray = new MyStackArray<Integer>();
        stackArray.push(1);
        stackArray.push(2);
        stackArray.push(3);
        stackArray.push(3);
        stackArray.push(4);

        System.out.println(stackArray.pop());

        MyStackList<Integer> stackList = new MyStackList<Integer>();
        stackList.push(1);
        stackList.push(2);
        stackList.push(3);
        stackList.push(3);
        stackList.push(4);
    }

}
