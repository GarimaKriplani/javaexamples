package com.Testers;
import java.util.*;
import java.lang.*;
import java.io.*;

public class SortPractice {
/*to return k largest items from an array or file
example
Input:
2 -> no of test cases
5 2 -> 1st test case, 5 is size(n), 2 is k
12 5 787 1 23
7 3 -> 2nd test case, 7 is size, 3 is k
1 23 12 9 30 2 50

Output:
787 23
50 30 23
 */
    public static void main(String []args){
        //code
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter no of test cases");
        int testcase=sc.nextInt();
        int i,j;
        for(int h=0;h<testcase;h++){
            System.out.println("Testcase no "+(h+1));

            System.out.println("Enter no. of elements");
            int n=sc.nextInt();//no of elements
            System.out.println("Enter k items to be returned");
            int k=sc.nextInt();//no of kth elements
            System.out.println("Enter items :");
            int a[]=new int[n];
            // for taking input
            for(j=0;j<n;j++){
                a[j]=sc.nextInt();
            }
            //sorting array
            System.out.println("sorting now!! Result is :");
            for(i=0;i<n-1;i++){

                for(j=i+1;j<n;j++){
                    if(a[i]<a[j]){
                        int tmp=a[j];
                        a[j]=a[i];
                        a[i]=tmp;
                    }
                }
            }
            for(i=0;i<k;i++){
                System.out.print(a[i]+" ");
            }
            System.out.println();
        }
    }

}
