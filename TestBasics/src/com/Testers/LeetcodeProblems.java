package com.Testers;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.ArrayList;
import java.util.List;
public class LeetcodeProblems {

    public static void main(String[] args) {
        String[][] equations = {
        {"a", "b"}, {"b", "c"}};
        double [] values = {2.0, 3.0};

        String [] []queries = {{"a", "c"},
                {"b", "a"},
                {"a", "e"}, {"a", "a"}, {"x", "x"}
             };
        /*The input is:
             vector<pair<string, string>> equations,
             vector<double>& values,
             vector<pair<string, string>> queries ,
              where equations.size() == values.size()
        Return vector<double>.
         */
        double []res =calcEquation(equations,values,queries);
        for (double d: res){
            System.out.println(d);
        }

        /** merge 2 sorted int arrays into first **/
        System.out.println("** merge 2 sorted int arrays into first **");
        int nums1[]={1,2,3,0,0,0};
        int nums2[]={2,5,6};

        mergeSortedArraysIntoOne(nums1,nums2,3,3);



        /**remove duplicates from sorted array that appear more than twice**/
        System.out.println("***remove duplicates from sorted array***");
        int nums[]= {1,1,1,2,2,3};
        int len1=removeDuplicatesFromSortedArray(nums);
        for(int i=0;i<len1;i++){
            System.out.print(nums[i]);
        }
        System.out.println();


        /***Print all substrings of a given string**/
        System.out.println("**Print all substrings of a given string**");
        String str="abc";
        List<String> allSubstrings= new ArrayList<String>();
        int n=str.length();
        for(int len=1;len<=n;len ++)
        {
            for(int i=0;i<=n-len;i++)
            {
                int j=i+len-1;
                for(int k=i;k<=j;k++)
                { System.out.print(str.charAt(k));
                    //allSubstrings.add(String.valueOf(str.charAt(k)));
                }
                    System.out.println();

            }
        }
        for(String s:allSubstrings){
            System.out.println(s);
        }

    }
    public static void mergeSortedArraysIntoOne(int []A, int []B,int m, int n){
        while(m > 0 && n > 0){
            if(A[m-1] > B[n-1]){
                A[m+n-1] = A[m-1];
                m--;
            }else{
                A[m+n-1] = B[n-1];
                n--;
            }
        }

        while(n > 0){
            A[m+n-1] = B[n-1];
            n--;
        }

        for(int i:A){
            System.out.print(A[i]+",");
        }
    }


    public static int removeDuplicatesFromSortedArray(int []nums){
        int i = 0, j = -1, curCnt = 0;
        for (; i < nums.length; i++)
        {
            if (j < 0 || nums[i] != nums[j])
            {
                nums[++j] = nums[i];
                curCnt = 1;
            } else if (curCnt < 2)
            {
                nums[++j] = nums[i];
                curCnt++;
            }
        }
        return j + 1;

    }
    public static double[] calcEquation(String[][] equations, double[] values, String[][] query) {
        double[] result = new double[query.length];
        // filter unexpected words

        Set<String> words = new HashSet<>();
        for (String[] aEquation : equations) {
            words.add(aEquation[0]);
            words.add(aEquation[1]);
        }
        for (int i = 0; i < query.length; ++i) {
            String[] keys = query[i];
            if (!words.contains(keys[0]) || !words.contains(keys[1])) result[i] = -1.0d;
            else {
                Stack<Integer> stack = new Stack<>();
                result[i] = helper(equations, values, keys, stack);
            }
        }
        //System.out.print("result = " +result);
        return result;
    }
    public static double helper(String[][] equations, double[] values, String[] keys, Stack<Integer> stack) {
        // 直接查找，key的顺序有正反
        // look up equations directly
        for (int i = 0; i < equations.length; ++i) {
            if (equations[i][0].equals(keys[0]) && equations[i][1].equals(keys[1])) return values[i];
            if (equations[i][0].equals(keys[1]) && equations[i][1].equals(keys[0])) return 1 / values[i];
        }
        // lookup equations by other equations

        for (int i = 0; i < equations.length; ++i) {
            if (!stack.contains(i) && keys[0].equals(equations[i][0])) {
                stack.push(i);
                double temp = values[i] * helper(equations, values, new String[]{equations[i][1], keys[1]}, stack);
                if (temp > 0) return temp;
                else stack.pop();
            }
            if (!stack.contains(i) && keys[0].equals(equations[i][1])) {
                stack.push(i);
                double temp = helper(equations, values, new String[]{equations[i][0], keys[1]}, stack) / values[i];
                if (temp > 0) return temp;
                else stack.pop();
            }
        }
        // 查不到，返回-1
        return -1.0d;
    }

}
