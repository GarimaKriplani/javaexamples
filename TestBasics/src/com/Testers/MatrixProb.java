package com.Testers;

public class MatrixProb {
    public static boolean isToeplitzMatrix(int[][] matrix) {
        int m=matrix.length;
        int n=matrix[0].length;

        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                if(i >0 && j>0 && matrix[i-1][j-1]!=matrix[i][j]){
                    return false;
                }
            }

        }
        return true;
    }
    public static void main(String []args){
        int [][] matrix={
            { 1,2,3,4},
            {5,1,2,3},
            {9,5,1,2}
        };
        boolean res= isToeplitzMatrix(matrix);
        System.out.print("res= "+res);
    }
}
