package com.Testers;

public class TreeExamples {

    class Node {
        int data;
        Node left;
        Node right;
    }

    boolean checkBST(Node root) {
        return isBSTUTil(root,Integer.MIN_VALUE,Integer.MAX_VALUE);
    }
    boolean isBSTUTil(Node node, int min, int max){
        if(node == null)
            return true;
        if(node.data <min || node.data >max)
            return false;
        return (isBSTUTil(node.left,min,node.data-1) &&
                (isBSTUTil(node.right,node.data+1,max)));
    }
}
