package com.Testers;
import java.util.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map.Entry;

public class exam {

   public static List<String> retrieveMostFrequentlyUsedWords(String literatureText,
                                                 List<String> wordsToExclude)
    {
        HashMap<String,Integer> wordCount = new HashMap<String,Integer>();
        List<String> resultList= new ArrayList<String>();
        StringTokenizer st= new StringTokenizer(literatureText, " ");
        while(st.hasMoreTokens()){
            String tmp =st.nextToken();
            if(!wordsToExclude.contains(tmp)) {
                if(wordCount.containsKey(tmp)){
                    wordCount.put(tmp,wordCount.get(tmp)+1);
                }
              else{
                    wordCount.put(tmp,1);
                }
            }
        } //end of while to generate wordCount Map
        Set<Entry<String,Integer>> aSet =wordCount.entrySet();
        List<Entry<String,Integer>> aList= new ArrayList<Entry<String,Integer>>(aSet);
        Collections.sort(aList,new Comparator<Map.Entry<String,Integer>>(){
            public int compare(Map.Entry<String,Integer> a, Map.Entry<String,Integer> b){
                return (b.getValue()).compareTo(a.getValue());
            }
        });
        Map.Entry<String,Integer> maxentry= null;

        for(Map.Entry<String,Integer> entry:aList){
                if(maxentry ==null || entry.getValue().compareTo(maxentry.getValue())>=0){
                    maxentry =entry;
                    System.out.println(maxentry.getKey());
                    resultList.add(entry.getKey());
                }

        }
       /*for(Map.Entry<String,Integer> emax: maxentry){
            resultList.add(emax.getKey());
       }*/
        return resultList;
    }
    public static void main(String [] args){
            List<String> exclude= new ArrayList<String>();
            exclude.add("and");
            exclude.add("he");
            exclude.add("the");
            exclude.add("to");
            exclude.add("is");


            List<String> line = new ArrayList<String>();
            line.add("a1 9 2 3 1");
            line.add("g1 Act car");
            line.add("a8 act zoo");

            List<String>r =reorderLines(3,line);
        /* retrieveMostFrequentlyUsedWords("jack and jill went to the market" +
                    " to buy bread and cheese cheese is jack favorite food", exclude);*/
            for(String s:r){
                System.out.println(s);
            }


    }
    public static List<String> reorderLines(int logFileSize, List<String> logfile)
    {  /*for( String lines:logfile.){
        StringTokenizer aLine = new StringTokenizer(lines, " ");
           System.out.println(aLine.nextToken());
        System.out.println(aLine.nextToken());

        }// WRITE YOUR CODE HERE*/
        return null;
    }



}
