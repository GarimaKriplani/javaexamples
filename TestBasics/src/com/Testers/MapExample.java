package com.Testers;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;
public class MapExample {
    public static class MyEntry<K, V> {
        private final K key;
        private V value;

        public MyEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }


    public static class MyMap<K, V> {
        private int size;
        private int DEFAULT_CAPACITY = 16;
        @SuppressWarnings("unchecked")
        private MyEntry<K, V>[] entries = new MyEntry[DEFAULT_CAPACITY];


        public V get(K key) {
            for (int i = 0; i < size; i++) {
                if (entries[i] != null) {
                    if (entries[i].getKey().equals(key)) {
                        return entries[i].getValue();
                    }
                }
            }
            return null;
        }

        public void put(K key, V value) {
            boolean insert = true;
            for (int i = 0; i < size; i++) {
                if (entries[i].getKey().equals(key)) {
                    entries[i].setValue(value);
                    insert = false;
                }
            }
            if (insert) {
                resizeMap();
                entries[size++] = new MyEntry<K, V>(key, value);
                System.out.println("new size ="+ size);
            }
        }

        private void resizeMap()
        {
            if (size == entries.length) {
                int newSize = entries.length * 2;
                entries = Arrays.copyOf(entries, newSize); //SEE HERE
            }

        }

        public int size() {
            return size;
        }

        public void remove(K key) {
            for (int i = 0; i < size; i++) {
                if (entries[i].getKey().equals(key)) {
                    entries[i] = null;
                    size--;
                    condenseArray(i);
                }
            }
        }

        private void condenseArray(int start) {
            for (int i = start; i < size; i++) {
                entries[i] = entries[i + 1];
            }
        }

        public Set<K> keySet() {
            Set<K> set = new HashSet<K>();
            for (int i = 0; i < size; i++) {
                set.add(entries[i].getKey());
            }
            return set;
        }
    }

    public static void main(String[] args){
        MyMap<String,Integer> mymap=new MyMap<String,Integer>();
        mymap.put("Lars", 1);
        mymap.put("Lars", 2);
        mymap.put("Lars", 1);
        //System.out.println(mymap.get("Lars"));

        for (int i=0;i<5;i++){
            mymap.put(String.valueOf(i),i);

        }
        System.out.println("size=" +mymap.size()); //11

        for(int i=0;i<mymap.size();i++){
           // System.out.println(i + " -> " +mymap.get(String.valueOf(i)));
        }
    }

}
