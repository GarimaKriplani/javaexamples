package com.Testers;

 public class MySingleton {
     public static void main(String []args){
         SingleClass a = SingleClass.getInstance();
         SingleClass b = SingleClass.getInstance();
         System.out.println("Value of a.x before = " + a.x);
         System.out.println("Value of b.x before = " + b.x);
         a.x = a.x + 10;
         System.out.println("Value of a.x = " + a.x);
         System.out.println("Value of b.x = " + b.x);
     }
}
class SingleClass {
    static SingleClass instance = null;
    public int x = 10;

    // private constructor can't be accessed outside the class
    private SingleClass() {  }

    // Factory method to provide the users with instances
    static public SingleClass getInstance()
    {
        if (instance == null)
            instance = new SingleClass();

        return instance;
    }
}
