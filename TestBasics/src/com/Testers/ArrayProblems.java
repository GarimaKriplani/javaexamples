package com.Testers;
import java.util.*;

public class ArrayProblems {
   /*
   Given an array of integers nums, write a method that returns the "pivot" index of this array.
   We define the pivot index as the index where the sum of the numbers to the left of the index is equal to
   the sum of the numbers to the right of the index.
   If no such index exists, we should return -1. If there are multiple pivot indexes, you should return the
   left-most pivot index.
    */
    public static int pivotIndex(int[] nums){
        int len=nums.length;
        int pivotIndex=-1;
        for (int i=0;i<=len-1;i++){
            int sumleft;
            if(i ==0){
                sumleft =0;
            }
            else
                sumleft =getSum(nums,0,i-1);

            int sumright=getSum(nums,i+1,len-1);
            if(sumleft == sumright){
                pivotIndex=i;
               break;
            }
        }
        return pivotIndex;
    }

    public static int getSum(int[] nums,int startIndex, int endIndex){
        int sum=0;
        for(int i=startIndex;i<=endIndex;i++){
            sum +=nums[i];
        }

        return sum;
    }

    /* LeetCode
    Given a non-empty array of digits representing a non-negative integer, plus one to the integer.

    The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.

    You may assume the integer does not contain any leading zero, except the number 0 itself.
     Input: [1,2,3]
     Output: [1,2,4]
     Explanation: The array represents the integer 123.
     */

    public static int[] plusOne(int[] digits) {
        int carry=0;
        int len= digits.length;
        for (int i=len-1;i>=0;i--){

            if(i==len-1 || i==0){
                digits[i]=digits[i]+1;
            }
            digits[i]=digits[i]+carry;
            if(digits[i]==10){
                carry=1;
                digits[i]=0;
            }
            else if(digits[i]>10){
                int temp=digits[i]-10;
                digits[i]=temp;
                carry=1;
            }
            if(i==0){
            int[] newArr = Arrays.copyOf(digits, len+1);
            System.out.println("new array size= "+newArr.length);

            newArr[i] =carry;
            return newArr;
        }
    }
        return digits;
    }

    public static void main(String []args) {
       /* int nums[] ={-1,-1,-1,0,1,1};
        int res=pivotIndex(nums);
       // System.out.println(res);
        int nums1[]={9};

        int res1[]=plusOne(nums1);
        for(int r:res1)
        System.out.println(r);*/

        String str = "pcmbpcmbzz";


        StringBuffer sb= new StringBuffer(str);
        System.out.print(sb.toString());
        Set<Character> charSet = new LinkedHashSet<Character>();
      //  ArrayList<Set<Character>> allSets = new ArrayList<Set<Character>>();
        Set<Set<Character>> allSets = new LinkedHashSet<Set<Character>>();
        int cnt=0;

        for(int i=0;i<sb.length();i++){
            if(charSet.size()==5){
                //allSets.add(charSet);
                cnt=cnt+1;
                allSets.add(charSet);
                charSet=new LinkedHashSet<>(Collections.EMPTY_SET);
            }

            if(!charSet.contains(sb.charAt(i))){
                charSet.add(sb.charAt(i));
                sb.deleteCharAt(i);
            }
            if(i==sb.length())
                i=0;

        }
    }
        /*
        read each cahr
        does it exists in current set
         no-> add it , remove from string
         yes-> read next one
         return when size of set reaches 5 or reach end of string being read
         return what - current set/list


         */


/*static  ArrayList<String> getPerfectTeam(StringBuffer sb,int k,){
    ArrayList<String> res = new ArrayList<String>();
    Set<Character> set;
    getPerfectTeamHelper(sb,k,res, set);
    return res;
}*/

 /*static ArrayList<String> getPerfectTeamHelper(StringBuffer sb,int k,ArrayList<String> res, StringBuffer <char>charSet){


     for(int i=0;i<sb.length();i++){
         if(charSet.length() ==k){
             res.add(charSet.toString());

             break;
         }

         /*if(!charSet.con(sb.charAt(i))){
            // charSet.add(sb.charAt(i));
             charSet.
          //   charSet.add(Character.valueOf(sb.charAt(i)));
             sb.deleteCharAt(i);
             getPerfectTeamHelper(sb,k,res,charSet);
         }
     }

     return res;
 }*/
    static int printAllKLength(char[] set, int k)
    {
        int n = set.length;
        ArrayList<String> resultSet= new ArrayList<String>();
        printAllKLengthRec(set, "", n, k,resultSet);
        return resultSet.size();
    }

    // The main recursive method
// to print all possible
// strings of length k
    static void printAllKLengthRec(char[] set,
                                   String prefix,
                                   int n, int k,ArrayList<String> resultSet) {

        // Base case: k is 0,
        // print prefix
        if (k == 0 && (!resultSet.contains(prefix))) {
            System.out.println(prefix);
            resultSet.add(prefix);
            return;
        }

        // One by one add all characters
        // from set and recursively
        // call for k equals to k-1
        for (int i = 0; i < n; ++i) {

           if(!prefix.contains(String.valueOf(set[i]))) // Next character of input added only if not added
           {
               String newPrefix = prefix + set[i];

               // k is decreased, because
               // we have added a new character
               printAllKLengthRec(set, newPrefix,
                       n, k - 1, resultSet);
           }
        }
    }



  static int  permute(String str, int l ,int r){
        ArrayList<String> a= new ArrayList<String>();

      if (l == r)
          a.add(str);
          //System.out.println(str);
      else
      {
          for (int i = l; i <= r; i++)
          {
              str = swap(str,l,i);
              permute(str, l+1, r);
              str = swap(str,l,i);
          }
      }
      return a.size();
  }
    static public String swap(String a, int i, int j)
    {
        char temp;
        char[] charArray = a.toCharArray();
        temp = charArray[i] ;
        charArray[i] = charArray[j];
        charArray[j] = temp;
        return String.valueOf(charArray);
    }


}
