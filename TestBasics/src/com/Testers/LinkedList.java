package com.Testers;

public class LinkedList {

    Node head;
    class Node{
        int data;
        Node next;
        Node(int d){
            data =d;
            next= null;
        }

    }

    void deleteNode(int key){

    }
    public void removeDuplicates(){
        System.out.println(" In remove Duplicates");
        Node curr=head.next;
        Node prev=head;
        if(prev==null || curr == null)
            return ;
        Node temp=null;
        while(curr !=null){

            if(prev.data != curr.data)
            {
                prev=curr;
                curr=curr.next;
            }
           else
            {
                temp=curr.next;
                prev.next=temp;
                curr=temp;

            }

        }
        return;
    }
    public void addToLinkedList(int item){

        if(head ==null){
            head=new Node(item);
            head.next=null;
            return;
        }
        Node newNode = new Node(item);
        newNode.next =null;
        Node last=head;

        while(last.next!=null){
            last=last.next;
        }
        last.next=newNode;
        return;
    }
    public void printList(){
        Node curr=head;
        System.out.println("Printing Linked List ");
        while(curr!=null){
            System.out.print(curr.data+"->");
            curr=curr.next;
        }
    }
    public Node deleteDuplicates(Node head) {
        Node Head= head;
        Node curr=head.next;
        Node prev=head;
        if(prev==null || curr == null)
            return null;
        Node temp=null;
        while(curr !=null){
            if(prev.data!= curr.data)
            {
                prev=curr;
                curr=curr.next;
            }
            else
            {
                temp=curr.next;
                prev.next=temp;
                curr=temp;
            }
        }
        return Head;
    }
    public void insertNodeafterkey(int key, int item){
        Node curr=head;
        if(curr ==null ){
            return ;
        }

        while(curr !=null){
            if(curr.data ==key){
                Node prev=curr;
                Node nextNode=curr.next;
                Node newNode= new Node(item);
                prev.next=newNode;
                newNode.next=nextNode;
                curr=newNode;
            }
            curr=curr.next;
        }
    }

    public void reverseLinkedList(){

        Node prev=null;
        Node curr=head;
        Node nextNode=null;
        if(curr==null){
            return;
        }
        while(curr!=null){
            nextNode=curr.next;
            curr.next=prev;
            prev=curr;
            curr =nextNode;
        }
        head=prev;

    }
    public void swapNodes(int x, int y){
        if (x == y) return;

        // Search for x (keep track of prevX and CurrX)
        Node prevX = null, currX = head;
        while (currX != null && currX.data != x)
        {
            prevX = currX;
            currX = currX.next;
        }

        // Search for y (keep track of prevY and currY)
        Node prevY = null, currY = head;
        while (currY != null && currY.data != y)
        {
            prevY = currY;
            currY = currY.next;
        }

        // If either x or y is not present, nothing to do
        if (currX == null || currY == null)
            return;

        // If x is not head of linked list
        if (prevX != null)
            prevX.next = currY;
        else //make y the new head
            head = currY;

        // If y is not head of linked list
        if (prevY != null)
            prevY.next = currX;
        else // make x the new head
            head = currX;

        // Swap next pointers
        Node temp = currX.next;
        currX.next = currY.next;
        currY.next = temp;

    }
    public static void main(String []args){
        LinkedList linkedList = new LinkedList();
        linkedList.addToLinkedList(1);
        linkedList.addToLinkedList(2);
        linkedList.addToLinkedList(2);
        linkedList.addToLinkedList(3);
        linkedList.addToLinkedList(4);
        linkedList.insertNodeafterkey(1,99);

       // linkedList.removeDuplicates( linkedList.printList();
        linkedList.reverseLinkedList();
        linkedList.printList();
        linkedList.swapNodes(99,1);
        linkedList.printList();



    }
}
