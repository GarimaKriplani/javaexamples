package com.braviant;


    enum Side { NONE, LEFT, RIGHT }

    public class ChainLink {
        private ChainLink left, right;
        public int sizeOfThisLink(ChainLink aLink){
            int size=0;
            ChainLink start=aLink.left;
            while(start !=aLink.right){
                size++;
            }
            return size;
        }
        public void append(ChainLink rightPart) {
            if (this.right != null)
                throw new IllegalStateException("Link is already connected.");

            this.right = rightPart;
            rightPart.left = this;
        }

        public Side longerSide() {
            if(sizeOfThisLink(this.left)<sizeOfThisLink(this.right)){
                return Side.RIGHT;
            }
            else if(sizeOfThisLink(this.left)>sizeOfThisLink(this.right)){
                return Side.LEFT;
            }
            return Side.NONE;

        }

        public static void main(String[] args) {
            ChainLink left = new ChainLink();
            ChainLink middle = new ChainLink();
            ChainLink right = new ChainLink();
            left.append(middle);
            middle.append(right);
            System.out.println(left.longerSide());
        }
    }

